import React, { Component } from "react";
import {
  SafeAreaView,
  ScrollView,
  Switch,
  Text,
  Alert,
  View,
} from "react-native";
import CalendarStrip from "react-native-calendar-strip";
import { Button, Card } from "react-native-elements";
import { TouchableOpacity } from "react-native-gesture-handler";
import ImagePicker from "react-native-image-crop-picker";
import { homeStyles } from "../common/css";
import CourseList from "./CourseList";

const theme = {
  Button: {
    titleStyle: {
      color: "red",
    },
  },
};

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedDate: "",
      date: new Date(),
      course: [
        {
          courseName: "1",
          courseType: "(Theory)",
          stime: "11pm",
          endtime: "12pm",
          count: "6",
          timer: "01H00M00S",
        },
        {
          courseName: "2",
          courseType: "(Practical)",
          stime: "12pm",
          endtime: "01pm",
          count: "1",
          timer: "01H00M00S",
        },
        {
          courseName: "3",
          courseType: "(Practical)",
          stime: "03pm",
          endtime: "04pm",
          count: "1",
          timer: "01H00M00S",
        },
      ],
      start: false,
      id: "",
      modalVisible: false,
      isModalVisible: false,
    };

    this.handleSwitchChange = this.handleSwitchChange.bind(this);
    this.showConfirmDialog = this.showConfirmDialog.bind(this);
    this.takePhotoFromCamera = this.takePhotoFromCamera.bind(this);
  }

  takePhotoFromCamera = async () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then((image) => {
      console.log(image);
    });
  };

  handleSwitchChange = (i) => {
    if (this.state.start === false) {
      this.showConfirmDialog(i);
    } else {
      this.setState({
        isModalVisible: true,
        start: !this.state.start,
        id: i,
      });
    }
  };

  showConfirmDialog = (i) => {
    return Alert.alert(
      "Are you sure you want to start class?",
      "Confirm To Start The Class",
      [
        // The "Yes" button
        {
          text: "Start",
          onPress: () => {
            this.setState({
              start: !this.state.start,
              id: i,
            });
          },
        },
        // The "No" button
        // Does nothing but dismiss the dialog when tapped
        {
          text: "Cancel",
        },
      ]
    );
  };

  toggleModalVisibility = () => {
    this.setState({
      isModalVisible: false,
    });
  };

  render() {
    const { navigation } = this.props;
    return (
      <SafeAreaView>
        <View>
          <View style={homeStyles.listView}>
            <Text style={homeStyles.headtext}>Timesheet</Text>
          </View>
          <CalendarStrip
            scrollable
            calendarAnimation={{ type: "sequence", duration: 30 }}
            daySelectionAnimation={{
              type: "border",
              duration: 200,
              borderWidth: 1,
              borderHighlightColor: "#00a64f",
            }}
            style={homeStyles.celendarCom}
            calendarHeaderStyle={{
              color: "black",
              paddingTop: 10,
              fontSize: 15,
            }}
            calendarColor={"#7743CE"}
            dateNumberStyle={{ color: "black" }}
            dateNameStyle={{ color: "black" }}
            highlightDateNumberStyle={{ color: "#00a64f" }}
            highlightDateNameStyle={{ color: "#00a64f" }}
            disabledDateNameStyle={{ color: "#00a64f" }}
            disabledDateNumberStyle={{ color: "#00a64f" }}
            iconContainer={{ flex: 0.1 }}
            selectedDate={this.state.date}
            onDateSelected={(date) => this.setState({ selectedDate: date })}
          />
          <View style={homeStyles.listView}>
            <Text style={homeStyles.headtext}>Today Class List</Text>
          </View>
          <ScrollView showsVerticalScrollIndicator={false}>
            {this.state.course.map((data, i) => {
              return (
                <Card key={i}>
                  <TouchableOpacity
                    key={i}
                    onPress={() => navigation.navigate("CourseList", { courseType: data.courseType })} >
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginBottom: 10,
                      }}
                    >
                      <View style={homeStyles.cardTitle1}>
                        <Text style={{ color: "#00a64f" }}>
                          Course {data.courseName}
                        </Text>
                        <Text style={{ color: "#606060", marginLeft: 5 }}>
                          {data.courseType}
                        </Text>
                      </View>
                      <View style={homeStyles.cardTitle2}>
                        <View style={homeStyles.sView}>
                          <Text style={{ color: "#00a64f" }}>Start Time:</Text>
                          <Text style={{ color: "#00a64f", marginLeft: 5 }}>
                            {data.stime}
                          </Text>
                        </View>
                        <View style={homeStyles.sView}>
                          <Text style={{ color: "#00a64f", marginTop: 5 }}>
                            End Time:
                          </Text>
                          <Text
                            style={{
                              color: "#00a64f",
                              marginLeft: 13,
                              marginTop: 5,
                            }}
                          >
                            {data.endtime}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>

                  <Card.Divider />
                  <View style={homeStyles.cardContent}>
                    <View style={{ alignItems: "center" }}>
                      <Text style={homeStyles.textColor}>Student Count</Text>
                      <Text style={homeStyles.colorGreen}>{data.count}</Text>
                    </View>
                    <View style={{ alignItems: "center" }}>
                      <Text style={homeStyles.textColor}>Timer</Text>
                      <Text style={homeStyles.colorGreen}>{data.timer}</Text>
                    </View>
                    <View style={{ alignItems: "center" }}>
                      <Text style={homeStyles.textColor}>Start Classes</Text>
                      {this.state.id === i ? (
                        <Switch
                          onChange={() => this.handleSwitchChange(i)}
                          style={{ marginTop: 5 }}
                          value={this.state.start}
                          color="#00a64f"
                        />
                      ) : (
                        <Switch
                          onChange={() => this.handleSwitchChange(i)}
                          style={{ marginTop: 5 }}
                          value={false}
                          color="#00a64f"
                        />
                      )}
                    </View>
                  </View>
                </Card>
              );
            })}
          </ScrollView>
          {/* <Modal animationType="slide"
            transparent visible={this.state.isModalVisible}
            presentationStyle="overFullScreen"
            onDismiss={this.toggleModalVisibility}> */}
          {/* <View style={homeStyles.viewWrapper}>
              <View style={homeStyles.modalView}>

              <View style={{flexDirection: 'row'}}>
          
                <Input
                  style={homeStyles.textInput}
                  label='Start Miles'
                  placeholder='Enter Start Miles'
                />
                 <Icon 
                  style={homeStyles.icons}
                  name="camera" 
                  size={30}
                  onPress={this.takePhotoFromCamera}
                />
                
              </View> */}

          {/** This button is responsible to close the modal */}
          {/* <View style={{ flexDirection: 'row' }}>
                  <ThemeProvider theme={theme}>
                    <Button
                      title="Cancel"
                      titleStyle={{ color: '#00a64f' }}
                      buttonStyle={{ borderColor: '#00a64f', borderRadius: 20 }}
                      style={{ width: wp('30%') }}
                      type="outline"
                      onPress={this.toggleModalVisibility}
                    />
                    <Button
                      title="Save"
                      titleStyle={{ color: 'white' }}
                      buttonStyle={{ backgroundColor: '#00a64f', borderRadius: 20 }}
                      style={{ width: wp('30%'), marginLeft: 20 }}
                      onPress={this.toggleModalVisibility}
                    />
                  </ThemeProvider>
                </View>
                
              </View>
            </View> */}
          {/* </Modal> */}
        </View>
      </SafeAreaView >
    );
  }
}
