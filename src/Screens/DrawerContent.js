import React from "react";
import { View, TouchableOpacity } from "react-native";
import { Avatar, Title, Drawer, Text } from "react-native-paper";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import { drawerStyles } from "../common/css";
import Dashboard from "../Screens/Home";
import Icon from "react-native-vector-icons/MaterialIcons";
import AsyncStorage from "@react-native-community/async-storage";

export function DrawerContent(props) {
  const logOut = () => {
    AsyncStorage.clear();
    props.navigation.replace("Home");
  };
  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props}>
        <TouchableOpacity
          style={{ margin: 20 }}
          onPress={props.navigation.closeDrawer}
        >
          <Icon name="close" color={"white"} size={25} />
        </TouchableOpacity>
        <View style={drawerStyles.drawerContent}>
          <View style={drawerStyles.userInfoSection}>
            <View style={{ flexDirection: "row" }}>
              <Avatar.Image
                source={{
                  uri: "https://www.mcicon.com/wp-content/uploads/2021/01/People_User_1-copy-4.jpg",
                }}
                size={90}
              />
              <View
                style={{
                  marginLeft: 15,
                  flexDirection: "column",
                  justifyContent: "center",
                }}
              >
                <Title style={drawerStyles.title}>John Doe</Title>
                {/* <Caption style={drawerStyles.caption}>@j_doe</Caption> */}
              </View>
            </View>
          </View>

          <Drawer.Section style={drawerStyles.drawerSection}>
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="text-snippet" color={"white"} size={size} />
              )}
              label={() => <Text style={{ color: "white" }}>Timesheet</Text>}
              onPress={() => {
                props.navigation.navigate("Dashboard");
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="groups" color={"white"} size={size} />
              )}
              label={() => <Text style={{ color: "white" }}>Student List</Text>}
              onPress={() => {
                props.navigation.navigate("Student List");
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="person" color={"white"} size={size} />
              )}
              label={() => <Text style={{ color: "white" }}>Edit Profile</Text>}
              onPress={() => {
                props.navigation.navigate("Edit Profile");
              }}
            />
          </Drawer.Section>
        </View>
      </DrawerContentScrollView>
      <Drawer.Section style={drawerStyles.bottomDrawerSection}>
        <DrawerItem
          icon={({ size }) => (
            <Icon name="exit-to-app" color={"white"} size={size} />
          )}
          label={() => <Text style={{ color: "white" }}>Sign Out</Text>}
          onPress={() => logOut()}
        />
      </Drawer.Section>
    </View>
  );
}
