import React, { Component } from 'react'
import {
    SafeAreaView,
    ScrollView,
    Text,
    View,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Avatar, Input } from 'react-native-elements';
import Ripple from 'react-native-material-ripple';
import ImagePicker from 'react-native-image-crop-picker';
import { EPstyles } from '../common/css';

export default class EditProfile extends Component {

    constructor(props) {
        super(props)
        this.takePhotoFromCamera = this.takePhotoFromCamera.bind(this);
    }

    takePhotoFromCamera = async () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            console.log(image);
        });
    }

    render() {
        return (
            <SafeAreaView>
                <View>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={EPstyles.listView}>
                            <Avatar
                                size={100}
                                rounded
                                source={{
                                    uri:
                                        'https://www.mcicon.com/wp-content/uploads/2021/01/People_User_1-copy-4.jpg',
                                }}
                                onAccessoryPress={() => Alert.alert("change avatar")}
                            >
                                <Avatar.Accessory size={24} onPress={this.takePhotoFromCamera} />
                            </Avatar>

                            <Text style={EPstyles.text}>Change Profile Picture</Text>

                            <Input
                                label='First Name'
                                placeholder='User'
                                labelStyle={{ color: 'black', fontSize: 15 }}
                                style={{ fontSize: 15 }}
                            />
                            <Input
                                label='Middle Name'
                                placeholder='Middle Name'
                                labelStyle={{ color: 'black', fontSize: 15 }}
                                style={{ fontSize: 15 }}
                            />
                            <Input
                                label='Last Name'
                                placeholder='Last'
                                labelStyle={{ color: 'black', fontSize: 15 }}
                                style={{ fontSize: 15 }}
                            />
                            <Input
                                label='Mobile Number'
                                placeholder='1234567890'
                                keyboardType='numeric'
                                maxLength={12}
                                labelStyle={{ color: 'black', fontSize: 15 }}
                                style={{ fontSize: 15 }}
                            />
                            <Input
                                label='Email-Id'
                                placeholder='test@gmail.com'
                                labelStyle={{ color: 'black', fontSize: 15 }}
                                style={{ fontSize: 15 }}
                            />
                            <View style={{ flexDirection: 'row', width: wp('45%'), alignSelf: 'flex-start' }}>
                                <Input
                                    label='House/apt Number'
                                    placeholder='House/apt Number'
                                    labelStyle={{ color: 'black', fontSize: 15 }}
                                    style={{ fontSize: 15 }}
                                />
                                <Input
                                    label='Street Name'
                                    placeholder='Street Name'
                                    labelStyle={{ color: 'black', fontSize: 15 }}
                                    style={{ fontSize: 15 }}
                                />
                            </View>
                            <View style={{ flexDirection: 'row', width: wp('45%'), alignSelf: 'flex-start' }}>
                                <Input
                                    label='City'
                                    placeholder='Enter City'
                                    labelStyle={{ color: 'black', fontSize: 15 }}
                                    style={{ fontSize: 15 }}
                                />
                                <Input
                                    label='Province'
                                    placeholder='Enter Provience'
                                    labelStyle={{ color: 'black', fontSize: 15 }}
                                    style={{ fontSize: 15 }}
                                />
                            </View>
                            <View style={{ flexDirection: 'row', width: wp('30%'), alignSelf: 'flex-start' }}>
                                <Input
                                    label='Post Code'
                                    placeholder='Post Code'
                                    keyboardType='numeric'
                                    labelStyle={{ color: 'black', fontSize: 15 }}
                                    style={{ fontSize: 15 }}
                                />
                                <Input
                                    label='Code'
                                    placeholder='Code'
                                    keyboardType='numeric'
                                    labelStyle={{ color: 'black', fontSize: 15 }}
                                    style={{ fontSize: 15 }}

                                />
                                <Input
                                    label='Country'
                                    placeholder='Enter Contry'
                                    labelStyle={{ color: 'black', fontSize: 15 }}
                                    style={{ fontSize: 15 }}
                                />
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), marginLeft: 20 }}>
                            <Ripple rippleColor="#99FF99" rippleOpacity={0.9} style={EPstyles.saveBtn}>
                                <Text style={EPstyles.saveText}>Save</Text>
                            </Ripple>
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}