import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { Avatar, ListItem } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';
import { SLstyles } from '../common/css';

export default class StudentList extends Component {

    constructor() {
        super();
        this.state = {
            list: [
                {
                    name: 'Amy Farha',
                    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                    subtitle: '#123456'
                },
                {
                    name: 'Chris Jackson',
                    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
                    subtitle: '#1234567'
                },
            ],
            show: false,
            ready: false,
            id: ''
        }
    }

    showList = (i) => {
        this.setState({
            show: !this.state.show,
            id: i
        })
    }

    render() {
        return (
            <SafeAreaView>
                    <View style={{ flexDirection: 'column' }}>
                        {
                            this.state.list.map((l, i) => (
                                <View key={i} style={{ marginTop: 10 }}>
                                    <TouchableOpacity onPress={() => this.showList(i)} key={i} >
                                        <ListItem bottomDivider>
                                            {this.state.ready === true ?
                                                <View>
                                                    {this.state.id === i ?
                                                        <View style={SLstyles.leftReadyView}></View> :
                                                        <View style={SLstyles.leftNotReadyView}></View>
                                                    }
                                                </View>
                                                : <View style={SLstyles.leftNotReadyView}></View>}
                                            <Avatar
                                                size="small"
                                                rounded
                                                source={{ uri: l.avatar_url }}
                                                activeOpacity={0.7}
                                            />
                                            <ListItem.Content>
                                                <ListItem.Title style={{ fontSize: 15, color: 'black' }}>{l.name}</ListItem.Title>
                                                <ListItem.Subtitle style={{ fontSize: 13, color: 'grey' }}>{l.subtitle}</ListItem.Subtitle>
                                            </ListItem.Content>
                                            {this.state.ready === true ?
                                                <View>
                                                    {this.state.id === i ?
                                                        <Text style={{ textAlign: 'center', color: '#00a64f' }}>Ready To Test</Text> :
                                                        null
                                                    }
                                                </View>
                                                : null}
                                            <Image
                                                source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2019/02/arrow_right_icon.png' }}
                                                style={SLstyles.iconStyle} />
                                        </ListItem>
                                    </TouchableOpacity>
                                    {this.state.show === true ?
                                        <View>
                                            {this.state.id === i ?
                                                <View key={i} style={{ flexDirection: 'column', marginLeft: 5 }}>
                                                    <TouchableOpacity onPress={() => { this.setState({ ready: !this.state.ready, id: i }) }}>
                                                        <ListItem bottomDivider>
                                                            <View style={SLstyles.readyBtn}><Text style={SLstyles.btnText}>Ready To Test</Text></View>
                                                        </ListItem>
                                                    </TouchableOpacity>
                                                    <ListItem bottomDivider>
                                                        <ListItem.Content style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                            <ListItem.Title>Course 1 (Theory)</ListItem.Title>
                                                            <ListItem.Title>
                                                                <View style={SLstyles.cardTitle2}>
                                                                    <View style={SLstyles.sView}>
                                                                        <Text>Start Time:</Text>
                                                                        <Text style={{ marginLeft: 5 }}>11pm</Text>
                                                                    </View>
                                                                    <View style={SLstyles.sView}>
                                                                        <Text style={{ marginTop: 5 }}>End Time:</Text>
                                                                        <Text style={{ marginLeft: 13, marginTop: 5 }}>12pm</Text>
                                                                    </View>
                                                                </View>
                                                            </ListItem.Title>
                                                        </ListItem.Content>
                                                    </ListItem>
                                                    <ListItem bottomDivider>
                                                        <ListItem.Content style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                            <ListItem.Title>Course 1 (Theory)</ListItem.Title>
                                                            <ListItem.Title>
                                                                <View style={SLstyles.cardTitle2}>
                                                                    <View style={SLstyles.sView}>
                                                                        <Text>Start Time:</Text>
                                                                        <Text style={{ marginLeft: 5 }}>11pm</Text>
                                                                    </View>
                                                                    <View style={SLstyles.sView}>
                                                                        <Text style={{ marginTop: 5 }}>End Time:</Text>
                                                                        <Text style={{ marginLeft: 13, marginTop: 5 }}>12pm</Text>
                                                                    </View>
                                                                </View>
                                                            </ListItem.Title>
                                                        </ListItem.Content>
                                                    </ListItem>
                                                    <ListItem bottomDivider>
                                                        <ListItem.Content style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                            <ListItem.Title>Course 1 (Theory)</ListItem.Title>
                                                            <ListItem.Title>
                                                                <View style={SLstyles.cardTitle2}>
                                                                    <View style={SLstyles.sView}>
                                                                        <Text>Start Time:</Text>
                                                                        <Text style={{ marginLeft: 5 }}>11pm</Text>
                                                                    </View>
                                                                    <View style={SLstyles.sView}>
                                                                        <Text style={{ marginTop: 5 }}>End Time:</Text>
                                                                        <Text style={{ marginLeft: 13, marginTop: 5 }}>12pm</Text>
                                                                    </View>
                                                                </View>
                                                            </ListItem.Title>
                                                        </ListItem.Content>
                                                    </ListItem>
                                                </View> : null}
                                        </View>
                                        : null}
                                </View>
                            ))
                        }
                    </View>
            </SafeAreaView >
        )
    }
}

