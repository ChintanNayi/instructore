import { STUDENT_LIST, STUDENT_LIST_ERROR } from "../action-types"

const initialState = {
  studentList: null,
}

const studentListStore = (state = initialState, action) => {
  switch (action.type) {
    case STUDENT_LIST: {
      return {
        ...state,
        studentList: action.payload,
      };
    };
    case STUDENT_LIST_ERROR: {
      return {
        ...state,
        studentList: null,
      };
    };
    default:
      return state;
  }
}
