import { combineReducers, applyMiddleware, legacy_createStore as createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from 'redux-thunk';

//Login Reducers
import loginStore from "./loginReducer";
import sendOTPStore from "./loginReducer";
import typeOTPStore from "./loginReducer";
import forgotPasswordStore from "./loginReducer";
import forgotPasswordOTPStore from "./loginReducer";
import newPasswordStore from "./loginReducer";

const composedEnhancer = composeWithDevTools(applyMiddleware(thunkMiddleware));

const rootReducer = combineReducers({
    loginReducer: loginStore,
    sendOTPReducer: sendOTPStore,
    enterOTPReducer: typeOTPStore,
    forgotPasswordReducer: forgotPasswordStore,
    forgotPasswordOTPReducer: forgotPasswordOTPStore,
    newPasswordReducer: newPasswordStore
});

const configureStore = () => createStore(rootReducer, composedEnhancer);

export default configureStore;