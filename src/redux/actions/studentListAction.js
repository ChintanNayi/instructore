import axios from "axios";
import {
  STUDENT_LIST,
  STUDENT_LIST_ERROR,
  STUDENT_COURSE,
  STUDENT_COURSE_ERROR
} from "../action-types"

import {
  STUDENT_LIST_API,
  STUDENT_LIST_COURSE_API,
  config
} from "../const";

export const studentList = (userObj) => {
  const body = JSON.stringify(userObj);
  return (dispatch) => {
    axios.get(STUDENT_LIST_API, body, config)
      .then(response => {
        // console.log('Action Log', response);
        dispatch({
          type: STUDENT_LIST,
          payload: response.data,
        });
        return response;
      })
      .catch(error => {
        console.log('OTP Action Error Log', error);
        dispatch({
          type: STUDENT_LIST_ERROR,
          payload: error,
        });
      });
  }
}

export const studentCourse = (userObj) => {
  const body = JSON.stringify(userObj);
  return (dispatch) => {
    axios.get(STUDENT_LIST_COURSE_API, body, config)
      .then(response => {
        // console.log('Action Log', response);
        dispatch({
          type: STUDENT_COURSE,
          payload: response.data,
        });
        return response;
      })
      .catch(error => {
        console.log('OTP Action Error Log', error);
        dispatch({
          type: STUDENT_COURSE_ERROR,
          payload: error,
        });
      });
  }
}

