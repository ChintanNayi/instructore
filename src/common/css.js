import React from 'react'
import { StyleSheet } from 'react-native'
import { normalize } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
var { Platform } = React;

const wlcStyles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: (Platform.OS === 'ios') ? wp('55%') : wp('50%'),
        height: (Platform.OS === 'ios') ? hp('10%') : hp('10%'),
        marginTop: (Platform.OS === 'ios') ? '25%' : '15%',
    },
    mainImg: {
        width: wp('90%'),
        height: (Platform.OS === 'ios') ? hp('35%') : hp('40%'),
        marginTop: (Platform.OS === 'ios') ? '25%' : '25%'
    },
    textbottom: {
        height: wp('10%'),
        color: "#696969",
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
    }
});

const signInStyles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        // marginTop: 10,
    },
    head: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 30,
        paddingBottom: 30,
        fontSize: hp('4%'),
        color: '#4d4d4d'
    },
    title: {
        height: hp('4%'),
        marginBottom: 20,
        textAlign: 'center',
        color: 'grey',
        marginTop: 20
    },
    input: {
        height: hp('7%'),
        width: wp('80%'),
        borderColor: 'gray',
        borderBottomWidth: 1,
        margin: 10,
        color: 'black'
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        marginBottom: 10,
        backgroundColor: "#00a64f",
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: hp('9%'),
        marginTop: '17%',
        marginBottom: '10%'
    },
    forgot_button: {
        textAlign: 'right',
        flex: 1,
        marginRight: 40,
        marginTop: 20,
        color: "#696969",
    },
    iconStyle: {
        marginTop: 30,
        marginLeft: 10 // default is 12
    },
    searchSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    errorMsg: {
        color: "#FF0000",
        fontSize: 10,
    },
});

const sendOTPstyles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 10,
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    head: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: '8%',
        fontSize: 30,
        color: '#4d4d4d'
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: '12%',
        marginBottom: 10,
        backgroundColor: "#00a64f",
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: hp('9%'),
        marginTop: (Platform.OS === 'ios') ? '24%' : '15%',
        marginBottom: '10%',
    },
    iconStyle: {
        marginTop: 30,
        marginLeft: 10 // default is 12
    },
    searchSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    radioGroup: {
        flexDirection: 'row',
        width: wp('80%'),
    },
    radioText: {
        marginTop: 7,
        textAlign: 'left'
    },
    buttonContainer: {
        flexDirection: 'row',
        marginBottom: 20,

    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#00a64f',
        alignItems: 'center',
        justifyContent: 'center',
    },
    checkedCircle: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: '#00a64f',
    },
});

const EntStyles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 10,
    },
    head: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 40,
        fontSize: 25,
        color: '#4d4d4d'
    },
    subHead: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: '3%',
        color: '#007419',
        paddingBottom: '8%'
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: '15%',
        backgroundColor: "#00a64f",
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: (Platform.OS === 'ios') ? hp('9%') : hp('10%'),
        marginTop: (Platform.OS === 'ios') ? '24%' : '15%',
        marginBottom: '10%',
    },
    iconStyle: {
        marginTop: 30,
        marginLeft: 10 // default is 12
    },
    inputGroup: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 40
    },
    resend: {
        textAlign: 'center',
        color: '#00a64f'
    },
    resend1: {
        textAlign: 'center',
        color: 'grey'
    },
    underlineStyleBase:
    {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 3,
        color: 'black'
    },
    underlineStyleHighLighted:
    {
        borderColor: 'black',
        borderBottomColor: "#50ad50",
    },
    timerView: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 30,
        marginBottom: 20
    }
});

const homeStyles = StyleSheet.create({
    head: {
        backgroundColor: '#1c1c1c',
        flexDirection: 'row',
        textAlign: 'center'
    },
    image: {
        width: wp('10%'),
        height: hp('5%'),
        marginLeft: 20,
    },
    headText: {
        color: 'black',
        marginTop: 10,
        marginLeft: 90
    },
    listView: {
        alignContent: 'center',
        alignItems: 'center'
    },
    headtext: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
        color: '#00a64f',
        marginTop: 10
    },
    cardTitle1: {
        flexDirection: 'row',
        marginTop: 10
    },
    cardTitle2: {
        flexDirection: 'column',
    },
    sView: {
        flexDirection: 'row',
        marginLeft: 1,
    },
    cardContent: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        textAlign: 'center'
    },
    viewWrapper: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(0, 0, 0, 0.2)",
    },
    modalView: {
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        top: "40%",
        left: "5%",
        elevation: 5,
        backgroundColor: "#fff",
        borderRadius: 7,
        width: wp('90%'),
        height: hp('30%')
    },
    icons: {
        marginTop: 10,
    },
    textInput: {
        width: wp('5%'),
        marginBottom: 8,
        marginLeft: 15,
    },
    celendarCom: {
        height: hp('10%'),
        width: wp('90%'),
        backgroundColor: 'white',
        marginLeft: 20,
        marginTop: 10,
    },
    textColor: {
        color: 'black'
    },
    colorGreen: {
        color: '#00a64f',
        paddingTop: 5
    }

});

const SLstyles = StyleSheet.create({
    iconStyle: {
        width: 30,
        height: 30,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    cardTitle2: {
        flexDirection: 'column',
    },
    sView: {
        flexDirection: 'row',
        marginLeft: 1,
    },
    readyBtn: {
        marginLeft: '35%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#00a64f',
        width: wp('30%'),
        height: hp('3%'),
        textAlignVertical: "center",
        backgroundColor: '#00a64f',
    },
    btnText: {
        fontSize: normalize(12),
        color: 'white',
        textAlign: 'center',
        paddingTop: (Platform.OS === 'ios') ? '2%' : '0%',
    },
    leftReadyView: {
        backgroundColor: '#00a64f',
        height: hp('6%'),
        width: wp('3%'),
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
    },
    leftNotReadyView: {
        backgroundColor: '#feba24',
        height: hp('6%'),
        width: wp('3%'),
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    }

});

const EPstyles = StyleSheet.create({
    text: {
        padding: 10,
        color: '#808080',
    },
    listView: {
        alignContent: 'center',
        alignItems: 'center',
        padding: 10

    },
    saveText: {
        color: 'white',
        fontWeight: '500',
    },
    saveBtn:
    {
        width: wp('90%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        // marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#00a64f",
    },

});

const drawerStyles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
        color: 'white',
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    caption: {
        color: 'white',
        fontSize: 14,
        lineHeight: 14,
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        color: 'white',
        fontWeight: 'bold',
        marginRight: 3,
    },
    drawerSection: {
        marginTop: 15,
    },
    bottomDrawerSection: {
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
        // color: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
});

const CLstyles = StyleSheet.create({
    listView: {
        alignContent: 'center',
        alignItems: 'center'
    },
    headtext: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
        color: '#00a64f',
        marginTop: 20,
        marginBottom: 10
    },
    cardContent: {
        flexDirection: 'row',
        textAlign: 'center',
        justifyContent: 'space-between'
    },
    viewWrapper: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(0, 0, 0, 0.2)",
    },
    modalView: {
        position: "absolute",
        elevation: 5,
        top: '35%',
        backgroundColor: "#fff",
        borderRadius: 7,
        width: wp('100%'),
        height: hp('70%')
    },
    celendarCom: {
        height: hp('10%'),
        width: wp('90%'),
        backgroundColor: 'white',
        marginLeft: 20,
        marginTop: 10,
        padding: 5
    }
});


export {
    wlcStyles,
    signInStyles,
    sendOTPstyles,
    EntStyles,
    homeStyles,
    SLstyles,
    EPstyles,
    drawerStyles,
    CLstyles
}
