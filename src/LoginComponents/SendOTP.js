import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, ScrollView } from "react-native";
import LOGO from "../assets/img/dsc-main-logo.png";
import Ripple from "react-native-material-ripple";
import { sendOTPstyles } from "../common/css";
import { Spinner } from "native-base";
import Toast from 'react-native-toast-message';

import { connect } from "react-redux";
import { loginOTP } from "../redux/actions/loginAction";

export class SendOTP extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checked: "first",
      options: [
        {
          key: "mail",
          text: "Email Id",
        },
        {
          key: "number",
          text: "Phone Number",
        },
      ],
      selectedOption: "mail",
    };
    this.onSelect = this.onSelect.bind(this);
    this.sendOTP = this.sendOTP.bind(this);
  }

  //Toast Message
  showToast = () =>
    Toast.show({
      visibilityTime: 2000,
      position: 'top',
      text1: 'OTP Sent Successfully!',
      topOffset: Platform.OS === 'ios' ? 40 : 5,
    });

  onSelect = (item) => {
    this.setState({ selectedOption: item.key });
    console.log("test", this.state.selectedOption);
    if (this.state.selectedOption === item.key) {
      this.setState({ selectedOption: null });
    } else {
      this.setState({ selectedOption: item.key });
    }
  };

  sendOTP = async () => {
    await this.props.dispatchState(this.state.email);

    this.setState({ loaded: true });
    // if (!this.props.otpState.error) {
    this.showToast();
    setTimeout(() => {
      this.setState({ loaded: false });
      this.props.navigation.navigate("EnterOTP");
    }, 3000);
    // }
  };

  render() {
    return (
      <ScrollView>
        <View style={sendOTPstyles.container}>
          <View style={{ alignItems: "center" }}>
            <Image style={sendOTPstyles.image} source={LOGO} />
          </View>
          <Text style={sendOTPstyles.head}>Get One-Time Code</Text>
          <View
            style={{ alignSelf: "flex-start", marginLeft: 60, marginTop: 70 }}
          >
            {this.state.options.map((item) => {
              return (
                <View key={item.key} style={sendOTPstyles.buttonContainer}>
                  <TouchableOpacity
                    style={sendOTPstyles.circle}
                    onPress={() => {
                      this.onSelect(item);
                    }}
                  >
                    {this.state.selectedOption === item.key && (
                      <View style={sendOTPstyles.checkedCircle} />
                    )}
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      this.onSelect(item);
                    }}
                  >
                    <Text style={{ marginLeft: 20, color: "black" }}>
                      {item.text}
                    </Text>
                  </TouchableOpacity>
                </View>
              );
            })}
          </View>
          {this.state.loaded === true ? (
            <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
          ) : (
            <Ripple
              rippleColor="#99FF99"
              rippleOpacity={0.9}
              onPress={() => this.sendOTP()}
              style={sendOTPstyles.loginBtn}
            >
              <Text style={sendOTPstyles.loginText}>Send</Text>
            </Ripple>
          )}
        </View>
        <Toast />
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  const { oneTime } = state.sendOTPReducer;
  return { otpState: oneTime };
}

const mapDispatchToProps = (dispatch) => (
  {
    dispatchState: (email) => dispatch(loginOTP(email))
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(SendOTP);
