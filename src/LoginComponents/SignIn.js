import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
} from "react-native";
import { Input, Icon } from "react-native-elements";
import { SafeAreaView } from "react-native-safe-area-context";
import LOGO from "../assets/img/dsc-main-logo.png";
import Ripple from "react-native-material-ripple";

import { signInStyles } from "../common/css";
import { Spinner } from "native-base";
import Toast from 'react-native-toast-message';

import { connect } from "react-redux";
import { loginInstructor } from "../redux/actions/loginAction";

export class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      isValidEmail: true,
      isValidPassword: true,
      isSecureEntry: true,

    };
  }

  //Email Input with Validation
  // inputEmailChange = (text) => {
  //   if (this.state.email.trim().length >= 4) {
  //     this.setState({
  //       email: text,
  //       isValidEmail: true,
  //     });
  //   } else {
  //     this.setState({
  //       email: text,
  //       isValidEmail: false,
  //     });
  //   }
  // };

  // //Password Input With Validation
  // inputPasswordChange = (text) => {
  //   if (this.state.password.trim().length >= 8) {
  //     this.setState({
  //       password: text,
  //       isValidPassword: true,
  //     });
  //   } else {
  //     this.setState({
  //       password: text,
  //       isValidPassword: false,
  //     });
  //   }
  // };

  // //On End Editing Input Email Validation
  // handleValidEmail = () => {
  //   if (this.state.email.trim().length >= 4) {
  //     this.setState({
  //       isValidEmail: true,
  //     });
  //   } else {
  //     this.setState({
  //       isValidEmail: false,
  //     });
  //   }
  // };

  showToast = () =>
    Toast.show({
      visibilityTime: 3000,
      position: 'top',
      text1: 'You have signed in successfully.',
      topOffset: 2,
    });

  SignIn = async () => {

    this.setState({ loaded: true });
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (
      reg.test(this.state.email) === true &&
      this.state.password.trim().length >= 8
    ) {
      this.setState({
        isValidEmail: true,
        isValidPassword: true,
      });
      await this.props.stateDispatch(this.state.email, this.state.password);
      // if (!this.props.loginState.error) {
      this.showToast();
      setTimeout(() => {
        this.setState({ loaded: false });
        this.props.navigation.navigate("SendOTP");
      }, 4000);
      // }
    }
    else {
      this.setState({
        isValidEmail: false,
        isValidPassword: false,
      });
      this.setState({ loaded: false });
    }
  };

  render() {
    return (
      <ScrollView>
        <SafeAreaView style={signInStyles.container}>
          <View style={{ alignItems: "center" }}>
            <Image style={signInStyles.image} source={LOGO} />
          </View>

          <Text style={signInStyles.head}>Sign In</Text>

          <Input
            containerStyle={{ width: '85%' }}
            style={{ fontSize: 14 }}
            placeholder="Enter User Name"
            leftIcon={{ type: 'font-awesome', name: 'user-o', color: 'grey' }}
            onChangeText={(text) => this.setState({ email: text })}
            errorMessage={this.state.isValidEmail ? null : ('Please enter a valid username.')}
          />
          {/* {this.state.isValidEmail ? null : (
            <View style={{ paddingRight: 188 }}>
              <Text style={signInStyles.errorMsg}>
                Please enter a valid username.
              </Text>
            </View>
          )} */}

          <Input
            secureTextEntry={this.state.isSecureEntry}
            containerStyle={{ width: '85%' }}
            style={{ fontSize: 15, right: 20 }}
            placeholder="Enter Password"
            leftIcon={{ type: 'evilicon', name: 'unlock', size: 40, color: 'grey' }}
            leftIconContainerStyle={{ right: 10 }}
            rightIcon={
              <TouchableOpacity onPress={() => this.setState({ isSecureEntry: !this.state.isSecureEntry })}>
                <Icon name='eye' type='font-awesome' size={20} color='grey'>{this.state.isSecureEntry}</Icon>
              </TouchableOpacity>
            }
            onChangeText={(text) => this.setState({ password: text })}
            errorMessage={this.state.isValidPassword ? null : ('Password must be at least 8 characters.')}
          />
          {/* {this.state.isValidPassword ? null : (
            <View style={{ paddingRight: 145 }}>
              <Text style={signInStyles.errorMsg}>
                Password must be at least 8 characters.
              </Text>
            </View>
          )} */}

          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              style={{ flex: 1, justifyContent: "flex-end" }}
              onPress={() => this.props.navigation.navigate("ForgetPassword")}
            >
              <Text style={signInStyles.forgot_button}>Forgot Password?</Text>
            </TouchableOpacity>
          </View>

          {this.state.loaded === true ? (
            <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
          ) : (
            <Ripple
              rippleColor="#99FF99"
              rippleOpacity={0.9}
              onPress={this.SignIn}
              style={signInStyles.loginBtn}
            >
              <Text style={signInStyles.loginText}>SIGN IN</Text>
            </Ripple>
          )}
        </SafeAreaView>
        <Toast />
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  const { login } = state.loginReducer;
  return { loginState: login };
}

const mapDispatchToProps = (dispatch) => ({
  stateDispatch: (email, password) => dispatch(loginInstructor(email, password))
});

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
