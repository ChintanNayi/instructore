import React, { Component } from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Image, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import LOGO from '../assets/img/dsc-main-logo.png'
import { Input, Icon } from 'react-native-elements';
import Ripple from 'react-native-material-ripple';
import { Spinner } from 'native-base';
import Toast from 'react-native-toast-message';

import { connect } from "react-redux";
import { newPassword } from '../redux/actions/loginAction';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 100
    },
    head: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 50,
        paddingBottom: 30,
        fontSize: hp('3%'),
        color: '#4d4d4d'
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        marginBottom: 10,
        backgroundColor: "#00a64f",
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: hp('9%'),
        marginBottom: 10,
        marginTop: 5
    },
});

export class EnterNewPassword extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            isValidPassword: true,
            isSecureEntry: true,
            isNewSecureEntry: true,
        }
    }

    showToast = () =>
        Toast.show({
            visibilityTime: 2000,
            position: 'top',
            text1: 'OTP Verified Successfully!',
            topOffset: Platform.OS === 'ios' ? 40 : 5,
        });

    CallToAction = async (text) => {
        await this.props.dispatchState(this.state.email);

        this.setState({ loaded: true });
        if (
            this.state.password.trim().length >= 8 &&
            this.state.confirmPassword.trim().length >= 8 &&
            this.state.password === this.state.confirmPassword
        ) {
            this.setState({
                confirmPassword: text,
                isValidPassword: true,
            });
            this.showToast();
            setTimeout(() => {
                this.setState({ loaded: false });
                this.props.navigation.navigate("Login");
            }, 3000);
        }
        else {
            this.setState({
                confirmPassword: text,
                isValidPassword: false,
            });
            this.setState({ loaded: false });
        }
    }

    render() {
        return (
            <ScrollView>
                <SafeAreaView style={styles.container}>

                    <View style={{ alignItems: 'center' }}>
                        <Image style={styles.image} source={LOGO} /></View>
                    <Text style={styles.head}>Enter New Password !</Text>
                    <View style={{ width: wp('90%') }}>
                        <Input
                            secureTextEntry={this.state.isSecureEntry}
                            containerStyle={{ width: '96%', paddingLeft: '6%' }}
                            style={{ fontSize: 15, right: 20 }}
                            placeholder="New Password"
                            leftIcon={{ type: 'evilicon', name: 'unlock', size: 40, color: 'grey' }}
                            leftIconContainerStyle={{ right: 10 }}
                            rightIcon={
                                <TouchableOpacity onPress={() => this.setState({ isSecureEntry: !this.state.isSecureEntry })}>
                                    <Icon name='eye' type='font-awesome' size={20} color='grey'>{this.state.isSecureEntry}</Icon>
                                </TouchableOpacity>
                            }
                            onChangeText={(text) => this.setState({ password: text })}
                            errorMessage={this.state.isValidPassword ? null : ('Password must be same and at least 8 characters.')}
                            errorStyle={{ fontSize: 10 }}
                        />
                        <Input
                            secureTextEntry={this.state.isNewSecureEntry}
                            containerStyle={{ width: '96%', paddingLeft: '6%' }}
                            style={{ fontSize: 15, right: 20 }}
                            placeholder="Confirm New Password"
                            leftIcon={{ type: 'evilicon', name: 'unlock', size: 40, color: 'grey' }}
                            leftIconContainerStyle={{ right: 10 }}
                            rightIcon={
                                <TouchableOpacity onPress={() => this.setState({ isNewSecureEntry: !this.state.isNewSecureEntry })}>
                                    <Icon name='eye' type='font-awesome' size={20} color='grey'>{this.state.isNewSecureEntry}</Icon>
                                </TouchableOpacity>
                            }
                            onChangeText={(text) => this.setState({ confirmPassword: text })}
                            errorMessage={this.state.isValidPassword ? null : ('Password must be same and at least 8 characters.')}
                            errorStyle={{ fontSize: 10 }}
                        />
                    </View>
                    {this.state.loaded === true ? (
                        <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
                    ) : (
                        <Ripple
                            rippleColor="#99FF99"
                            rippleOpacity={0.9}
                            onPress={() => this.CallToAction()}
                            style={styles.loginBtn}
                        >
                            <Text style={styles.loginText}>Call To Action</Text>
                        </Ripple>
                    )}
                </SafeAreaView>
                <Toast />
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    const { newPassword } = state.newPasswordReducer;
    return { newPasswordState: newPassword };
}

const mapDispatchToProps = (dispatch) => (
    {
        dispatchState: () => dispatch(newPassword())
    }
);

export default connect(mapStateToProps, mapDispatchToProps)(EnterNewPassword);
