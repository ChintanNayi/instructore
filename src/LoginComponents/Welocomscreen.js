import React, { Component } from "react";
import { View, Image, Text, ScrollView } from "react-native";
import TruckParking from "../assets/img/truck_parking.png";
import LOGO from "../assets/img/dsc-main-logo.png";
import Ripple from "react-native-material-ripple";
import { wlcStyles } from "../common/css";
import AsyncStorage from "@react-native-community/async-storage";

export class Welocomscreen extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    // Start counting when the page is loaded
    this.timeoutHandle = setTimeout(() => {
      // Add your logic for the transition
      AsyncStorage.getItem("isLogin").then((value) =>
        this.props.navigation.replace(
          value === null ? "Home" : "ScreenExternal"
        )
      );
    }, 3000);
  }

  componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
  }

  render() {
    return (
      <View style={wlcStyles.container}>
        <View style={{ alignItems: "center" }}>
          <Image style={wlcStyles.image} source={LOGO} />
        </View>
        <Image style={wlcStyles.mainImg} source={TruckParking} />

        <View style={wlcStyles.bottom}>
          <Text style={wlcStyles.textbottom}>
            @ 2021 Driving School Cloud. All rights reserved
          </Text>
        </View>
      </View>
    );
  }
}
export default Welocomscreen;
