import React from "react";
import { StyleSheet } from "react-native";

import LoginScreen from "./src/LoginComponents/SignIn";
import WelcomeScreen from "./src/LoginComponents/Welocomscreen";
import ForgetPassword from "./src/LoginComponents/ForgetPassword";
import EnterOTP from "./src/LoginComponents/EnterOTP";
import SendOTP from "./src/LoginComponents/SendOTP";
import EnterNewPassword from "./src/LoginComponents/EnterNewPassword";
import ForgotPasswordOTP from "./src/LoginComponents/ForgotPasswordOTP";

import Dashboard from "./src/Screens/Home";
import Welocomscreen from "./src/LoginComponents/Welocomscreen";
import EditPofile from "./src/Screens/EditProfile";
import StudentList from "./src/Screens/StudentList";
import CourseList from "./src/Screens/CourseList";
import { DrawerContent } from "./src/Screens/DrawerContent";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NativeBaseProvider } from "native-base";

const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();

const secondScreenStack = ({ navigation }) => {
  return (
    <Drawer.Navigator
      screenOptions={{
        drawerStyle: {
          backgroundColor: "#414141",
          opacity: 0.95,
        },
      }}
      drawerContent={(props) => <DrawerContent {...props} />}
      drawerStatusBarAnimation
    >
      <Drawer.Screen name="Dashboard" component={Dashboard} />
      <Drawer.Screen name="Edit Profile" component={EditPofile} />
      <Drawer.Screen name="Student List" component={StudentList} />
      <Drawer.Screen
        options={{ headerShown: false }}
        name="LogOut"
        component={Welocomscreen}
      />
    </Drawer.Navigator>
  );
};

const Home = () => {
  return (
    <NativeBaseProvider>
      <Stack.Navigator>
        <Stack.Screen
          options={{ headerShown: false }}
          name="Login"
          component={LoginScreen}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="ForgetPassword"
          component={ForgetPassword}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="EnterOTP"
          component={EnterOTP}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="SendOTP"
          component={SendOTP}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="ForgotPasswordOTP"
          component={ForgotPasswordOTP}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="NewPassword"
          component={EnterNewPassword}
        />
      </Stack.Navigator>
    </NativeBaseProvider>
  );
};

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Welcome"
          component={WelcomeScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Home"
          component={Home}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="CourseList"
          component={CourseList}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ScreenExternal"
          component={secondScreenStack}
          options={{
            title: "External Screen", //Set Header Title
            headerStyle: {
              backgroundColor: "#f4511e", //Set Header color
            },
            headerShown: false,
            headerTintColor: "#fff", //Set Header text color
            headerTitleStyle: {
              fontWeight: "bold", //Set Header text style
            },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: "600",
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: "400",
  },
  highlight: {
    fontWeight: "700",
  },
});

export default App;
